use bank;

select * from loan, borrower ;

select * from loan inner join borrower on loan.loan_number = borrower.loan_number;

select * from loan left outer join borrower on loan.loan_number = borrower.loan_number;

insert into borrower values ("Erdem","L-39");
select * from loan right outer join borrower on loan.loan_number = borrower.loan_number;

(select loan.loan_number,customer_name from loan left outer join borrower on loan.loan_number = borrower.loan_number)
union
(select loan.loan_number,customer_name from loan right outer join borrower on loan.loan_number = borrower.loan_number);